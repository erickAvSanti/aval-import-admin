module.exports = {
  	pluginOptions: {
    	i18n: {
      		locale: 'en',
      		fallbackLocale: 'en',
      		localeDir: 'locales',
      		enableInSFC: true
    	}
  	},
  	devServer: {
    	https: false,
  	},
	chainWebpack: config => {
	    config.module.rules.delete('eslint');
	}
}
