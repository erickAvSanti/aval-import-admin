import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	modals:{
  		login:false,
  	},
  },
  mutations: {
  	showModalLogin(state){
  		state.modals.login = true;
  	},
  	hideModalLogin(state){
  		state.modals.login = false;
  	},
  	setModalLogin(state,value){
  		state.modals.login = value;
  	},
  },
  actions: {

  }
})
