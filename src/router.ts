import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Covers from './views/Covers.vue'
import Category from './views/Category.vue'
import Products from './views/Products.vue'
import Options from './views/Options.vue'
import TermsOfServices from './views/TermsOfServices.vue'
import Users from './views/Users.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/covers',
      name: 'covers',
      component: Covers
    },
    {
      path: '/categories',
      name: 'categories',
      component: Category
    },
    {
      path: '/products',
      name: 'products',
      component: Products
    },
    {
      path: '/options',
      name: 'options',
      component: Options
    },
    {
      path: '/users',
      name: 'users',
      component: Users
    },
    {
      path: '/terms-of-services',
      name: 'terms-of-services',
      component: TermsOfServices
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
